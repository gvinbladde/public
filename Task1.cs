﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestJob2
{
    class Program
    {
        static List<Card> cards = new List<Card>();
        static List<Card> trueCards = new List<Card>();
        static int cnt = 0;

        static void Main(string[] args)
        {
            Test();
            Console.ReadLine();
        }

        static void Test()
        {
            cnt = 0;
            List<string> cities = new List<string>() { "Мельбурн", "Кельн", "Москва", "Париж", "Город Н1", "Город Н2", "ГородН3" };
            for (int i = 0; i < 7; i++)
            {
                CardsAdd(cities, i);
                Comb(trueCards, cards);
            }
            Console.WriteLine("все остальные тесты выполнены успешно");
        }

        static void CardsAdd(List<string> cities, int count)
        {
            trueCards.Clear();
            if (count > 6) count = 6;
            List<string> actualCts = cities.GetRange(0, count+1);

            foreach (var v in actualCts)
            {
                if (v != actualCts.Last())
                    trueCards.Add(new Card(v, actualCts[actualCts.FindIndex(x => x == v) + 1]));
            }
        }

        static void Comb(List<Card> list, List<Card> outList)
        {
            foreach (var v in list)
            {
                outList.Add(v);
                if (list.Count != 1)
                {
                    List<Card> t = list.GetRange(0, list.Count);
                    t.Remove(v);
                    Comb(t, outList);
                }
                else
                {
                    Sort(outList, cnt++);
                }
                outList.Remove(v);
            }
        }

        static void Sort(List<Card> cards, int id)
        {
            //sorting
            for (int i = 0; i < cards.Count; i++)
            {
                for (int j = i; j < cards.Count; j++)
                {
                    bool change = true;
                    for (int k = i; k < cards.Count; k++)
                    {
                        if (j != k)
                        {
                            if (cards[j].from == cards[k].to)
                            {
                                change = false;
                                break;
                            }
                        }
                    }
                    if (change && j != i)
                    {
                        Card t = cards[j];
                        cards[j] = cards[i];
                        cards[i] = t;
                    }
                }
            }

            //output
            if (!cards.SequenceEqual(trueCards))
            {
                Console.WriteLine("Test #{0} is not complete", id);
            }
        }

    }

    class Card
    {
        public string from;
        public string to;

        public Card() { }
        public Card(string _from, string _to)
        {
            from = _from;
            to = _to;
        }
    }
}
