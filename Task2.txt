SELECT ProductId, COUNT(ProductId) FROM
(
	SELECT ProductId FROM Sales s1
	WHERE DateCreated = 
	(
		SELECT MIN(DateCreated) FROM Sales s2
		WHERE s1.CustomerId = s2.CustomerId
	)
) product
GROUP BY product.ProductId;